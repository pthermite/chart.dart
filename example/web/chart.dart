library chart;

import 'dart:html';
import 'package:chart/chart.dart';


void main() {
  
  
  Line chart = new Line({
    'labels' : ["January","February","March","April","May","June","July","August"],
    'datasets' : [
                {
                  'fillColor' : "rgba(220,220,220,0.5)",
                  'strokeColor' : "rgba(0,0,0,1)",
                  'pointColor' : "rgba(220,220,220,1)",
                  'pointStrokeColor' : "#fff",
                  'pointDot' : false,
                  'data' : [1,4,5,2,5,6,3,7,3]
                },
                {
                  'fillColor' : "rgba(151,187,205,0.5)",
                  'strokeColor' : "rgba(151,187,205,1)",
                  'pointColor' : "rgba(151,187,205,1)",
                  'pointStrokeColor' : "#fff",
                  'data' : [3,-1,0,4,4,-2,4,1]
                },
                {
                  'fillColor' : "clear",
                  'strokeColor' : "rgba(151,187,205,1)",
                  'pointColor' : "rgba(151,187,205,1)",
                  'pointStrokeColor' : "#fff",
                  'pointDot' : false,
                  'data' : [-3,-3,-1,-4,-4,-3,-4,-1]
                }
    ]
  }, {
    'scaleOverride' : true, 
    'scaleMinValue' : 0.0, 
    'scaleMaxValue' : 35.0, 
    'scaleStepValue' : null, 
    'bezierCurve' : true, 
    'animation' : false,
    'titleText' : 'testest'     
    });
  
  
  Line chart2 = new Line({
      'labels' : ["1","2","3","4","5","6","7","8","9"],
      'datasets' : [
                  {
                    'fillColor' : "rgba(123,244,220,0.5)",
                    'strokeColor' : "rgba(220,220,220,1)",
                    'pointColor' : "rgba(220,220,220,1)",
                    'pointStrokeColor' : "#fff",
                    'data' : [1,1,0,0,1,1,1,0]
                  }
      ]

    }, {
      'animationEasing': 'easeOutElastic',
      'animation' : true,
      'bezierCurve' : true, 
      }
    );
  
  Bool chartBool = new Bool({
      'labels' : ["Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag","Sonntag"],
      'datasets' : [
                  {
                    'strokeColor' : "rgba(255,10,10,1)",
                    'data' : [{'x':0, 'y':0},{'x':2, 'y':0},{'x':3, 'y':1},{'x':20, 'y':0},{'x':30, 'y':0},{'x':40, 'y':0},{'x':45, 'y':0},{'x':63, 'y':0},{'x':100, 'y':0},{'x':123, 'y':1},{'x':128, 'y':0},{'x':130, 'y':0}]
                  }
                  ]

    }, {
      'animationEasing': 'easeOutElastic',
      'animation' : true
      }
    );
  
  Bar bar = new Bar({
                      'labels' : ["January","February","March","April","May","June","July"],
                      'datasets' : [
                                  {
                                    'fillColor' : "rgba(220,220,220,0.5)",
                                    'strokeColor' : "rgba(220,220,220,1)",
                                    'data' : [65,59,90,81,56,55,40]
                                  },
                                  {
                                    'fillColor' : "rgba(255,255,255,1)",
                                    'strokeColor' : "rgba(0,0,255,0.5)",
                                    'data' : [28,48,40,19,null,27,100]
                                  }
                                  ]

  }, null);
  
  Bar bar2 = new Bar({
                      'labels' : ["January","February","March","April","May","June","July"],
                      'datasets' : [
                                  {
                                    'fillColor' : "rgba(220,220,220,0.5)",
                                    'strokeColor' : "rgba(220,220,220,1)",
                                    'data' : [55,59,90,59,56,55,60]
                                  }]

  }, 
  {
    'titleText' : 'testest'     
  });

  DivElement containerBool = new DivElement();
  containerBool.style.height ='400px';
  containerBool.style.width =  '100%';
  document.body.children.add(containerBool);
  chartBool.show(containerBool);
  
  DivElement container = new DivElement();
  container.style.height ='400px';
  container.style.width =  '100%';
  document.body.children.add(container);
  chart.show(container);
  
  DivElement container2 = new DivElement();
  container2.style.height ='400px';
  container2.style.width =  '100%';
  document.body.children.add(container2);
  chart2.show(container2);
    
  DivElement container3 = new DivElement();
  container3.style.height ='400px';
  container3.style.width =  '100%';
  document.body.children.add(container3);
  bar.show(container3);
  
  DivElement container4 = new DivElement();
  container4.style.height ='400px';
  container4.style.width =  '100%';
  document.body.children.add(container4);
  bar2.show(container4);


}


