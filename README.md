Chart.dart
==========

This is a DartLang port of Chart.js by Nick Downie. For more informations to Chart.js see: http://www.chartjs.org/

Implemented Features
---------------------
  * Chart types:
    * Line-Chart
    * Bar-Chart
    * Bool-Chart
  * Features:
    * Animations
    * config and default config
    * The possibility to add unlimited points between two labels. 

  