part of chart;

/*
 * Bool charts are intended to display changing boolean values in a precise way. In contrast to line or bar charts, 
 * bool charts display value-changes in regard to specific times. 
 */
class Bool extends DartChart {
  
  int labelHop; //x-distance between two x-axis labels
  int widestXLabel;
  double xAxisLength;
  double yAxisPosX;
  double xAxisPosY;
  bool showLastStep;
  double scaleX;

  Bool(data, config):super(data, config) {

    defaultConfiguration({
        'scaleOverlay' : false, 
        'scaleStepValue' : null, 
        'scaleMinValue' : 0.0,
        'scaleMaxValue' : 1.0,
        'scaleMaxSteps' : 2,
        'scaleLineColor' : "rgba(0,0,0,.1)", 
        'scaleLineWidth' : 1, 
        'scaleShowLabels' : true, 
        'scaleLabel' : null, 
        'scaleFontFamily' : "'Verdana'", 
        'scaleFontSize' : 12, 
        'scaleFontStyle' : "normal", 
        'scaleFontColor' : "#666", 
        'titleText' : '',
        'titleFontFamily' : "'Verdana'", 
        'titleFontSize' : 16, 
        'titleFontStyle' : "normal", 
        'titleFontColor' : "#666", 
        'scaleShowGridLines' : true, 
        'scaleGridLineColor' : "rgba(0,0,0,.05)", 
        'scaleGridLineWidth' : 1, 
        'datasetStroke' : true, 
        'datasetStrokeWidth' : 2, 
        'datasetFill' : false,
        'animation' : true, 
        'animationSteps' : 60, 
        'animationEasing' : "easeOutQuart", 
        'onAnimationComplete' : null
    });
    
  }
  
  void show(Element parentNode){
    
    super.show(parentNode);

    setScale();
    
    calculateXAxisSize();
    
    animationLoop();    
    
  }

  @override
  Map calculateScaling(int drawingHeight) {
      
      Map<String, dynamic> scale = new Map<String, dynamic>();    

      //bool graphs can be 0 and 1 with no intermediate steps
      scale['graphMax'] = 1.0;
      scale['graphMin'] = 0.0;
      scale['digits'] = 0;
      scale['stepSize'] = 1.0;
  
      scale['steps'] = this.data['datasets'].length * 2 -1;
                     
      return scale;

    }
  
  
  calculateXAxisSize() {
    
    double longestText = 1.0;
    //if we are showing the labels
    if (getConfiguration('scaleShowLabels')) {
      this.context.font = getConfiguration('scaleFontStyle').toString() + " " + getConfiguration('scaleFontSize').toString() + "px " + getConfiguration('scaleFontFamily').toString();
      for (int i = 0; i < this.data['labels'].length; i++) {
        double measuredText = this.context.measureText(this.data['labels'][i].toString()).width;
        longestText = (measuredText > longestText) ? measuredText : longestText;
      }
    }
    
    double longestLabel = longestText;
    
    //also calculate the width of the scales, so they won't be cut off.
    for (int i = 0; i < calculatedScale['labels'].length; i++) {
      this.context.font = getConfiguration('scaleFontStyle').toString() + " " + getConfiguration('scaleFontSize').toString() + "px " + getConfiguration('scaleFontFamily').toString();
      double measuredText = this.context.measureText(calculatedScale['labels'][i]).width;
      longestText = (measuredText > longestText) ? measuredText : longestText;
    }
    
     int maxXValue = 0;    
    this.showLastStep = false;
    this.data['datasets'].forEach( (dataset) {
      dataset['data'].forEach( (value) {
        if (maxXValue < value['x'])
          maxXValue = value['x'];
      });
    });

    //usually we use "this.data['labels'].length -1" as n. By removing the -1 we add another Hop after the last label (like at HD graphs)
    this.labelHop = ((this.width.toDouble() - longestText -10)  / (this.data['labels'].length) - (longestLabel / 2) / (this.data['labels'].length) ).floor();
    
    this.yAxisPosX = longestText + 10;
    
    this.xAxisPosY = this.scaleHeight.toDouble() + getConfiguration('scaleFontSize')/2;
    
    this.xAxisLength = (this.labelHop * (this.data['labels'].length) - 10).toDouble();
    
    //scale X claes the xAxis according to the maxXValue
    this.scaleX = (this.labelHop * (this.data['labels'].length)) / maxXValue;

  }
  
  /**
   * 
   * draw one step which consists of one bool point[ 0 | 1 ] X and one faked point not X
   * 
   * pointBefore: the last point before this step
   * 
   * returns: the last point inside this step
   */
  Map drawStep(int previousY, int currentX, int currentY, int datasetCount, double animPc){  
    
    //if there is more than one dataset draw the first boolgraph from 0 to 1, the secon from 2 to 3...
    currentY = currentY + 2 * datasetCount; 
    
    if ( previousY == null ){
      //it's the first point
      this.chartLayerContext.moveTo(xPos(currentX), yPos(currentY, animPc));  
    } else if ( previousY == currentY){
      //the last point had the same y value so we can just draw a line between them
      this.chartLayerContext.lineTo(xPos(currentX), yPos(currentY, animPc));
    } else {
      //the last point had a different Y value than the current point so we have to 
      //add another point to get a streight line  
      this.chartLayerContext.lineTo(xPos(currentX), yPos(previousY, animPc));
      this.chartLayerContext.lineTo(xPos(currentX), yPos(currentY, animPc));
    }
    
    return { 'x':currentX, 'y':currentY};
       
  }

  void drawBoolLines(double animPc) {
    
    bool notZero = false;
    
    //for all: drawStep
    //then draw end if last is not null
    int datasetCount = 0;
    
    this.data['datasets'].forEach( (Map dataset){
        
        this.chartLayerContext.lineWidth = getConfiguration('datasetStrokeWidth');
                       
        //get the baseline and topline of the current dataset
        int baseline = 2 * datasetCount; 
        int topline = 1 + 2 * datasetCount; 
        
        //draw row background 
        if (this.data['datasets'].length > 1){
          this.chartLayerContext.beginPath();
          this.chartLayerContext.strokeStyle = "rgba(0,0,0,0)"; //transparent      
          this.chartLayerContext.moveTo(xPos(0), yPos(topline, animPc));  
          this.chartLayerContext.lineTo(this.yAxisPosX + this.xAxisLength + 10 + getConfiguration('datasetStrokeWidth').toDouble(), yPos(topline, animPc));
          this.chartLayerContext.stroke();
          this.chartLayerContext.lineTo(this.yAxisPosX + this.xAxisLength + 10 + getConfiguration('datasetStrokeWidth').toDouble(), yPos(baseline, animPc));
          this.chartLayerContext.stroke();        
          this.chartLayerContext.lineTo(xPos(0), yPos(baseline, animPc));
          this.chartLayerContext.stroke(); 
          this.chartLayerContext.closePath();
          this.chartLayerContext.fillStyle = "rgba(255,255,255,0.1)";
          this.chartLayerContext.fill();
        }
    
        //only draw chart if it is not null
        if (dataset['data'] != null && dataset['data'].length > 0){
        
          Map data = {
           'x' : null,
           'y' : null
          };
  
          //line draw background 
          if (getConfiguration('datasetFill')) {
            this.chartLayerContext.beginPath();
            this.chartLayerContext.strokeStyle = "rgba(0,0,0,0)"; //transparent      
            
            if (dataset['data'].first['x'] != 0){
              //first point is not on the very left
              this.chartLayerContext.moveTo(xPos(0), yPos(baseline, animPc));  
              data = {'x' : 0,'y' : baseline};
            }
            
            dataset['data'].forEach( (value){
              if (value['y'] != null)
                data = drawStep(data['y'], value['x'], value['y'], datasetCount, animPc);
            });
   
            this.chartLayerContext.lineTo(xPos(data['x']), yPos(baseline, 1.0));
            this.chartLayerContext.stroke();
            this.chartLayerContext.lineTo(this.yAxisPosX, yPos(baseline, 1.0));
            this.chartLayerContext.stroke();
            this.chartLayerContext.closePath();
  
            if (dataset['fillColor'] != 'clear'){
              this.chartLayerContext.fillStyle = dataset['fillColor'];
              this.chartLayerContext.fill();
            }
          }
  
          this.chartLayerContext.beginPath();
          this.chartLayerContext.strokeStyle = dataset['strokeColor'];
          //draw lines
          data = {'x' : null,'y' : null};
          dataset['data'].forEach( (value){
            if (value['y'] != null)
              data = drawStep(data['y'], value['x'], value['y'], datasetCount, animPc);
          });
  
          //close path
          this.chartLayerContext.stroke();
          this.chartLayerContext.closePath();     
        }

        datasetCount++;
    });    
  }
  
  
  double yPos(num value, double animPc) {
    
    return this.xAxisPosY - animPc * (calculateOffset(value.toDouble(), this.calculatedScale, this.scaleHop));
  
  }

  double xPos(num xValue) {
    
    return this.yAxisPosX + (this.scaleX * xValue);
    
  }

  //@override
  void drawScale() {
    
    //X axis line
    this.gridLayerContext.lineWidth = getConfiguration('scaleLineWidth');
    this.gridLayerContext.strokeStyle = getConfiguration('scaleLineColor');
    this.gridLayerContext.beginPath();
    this.gridLayerContext.moveTo(this.yAxisPosX + this.xAxisLength +10 , this.xAxisPosY);
    this.gridLayerContext.lineTo(this.yAxisPosX - 5 , this.xAxisPosY);
    this.gridLayerContext.stroke();

    if (this.rotateLabels > 0) {
      this.gridLayerContext.save();
      this.gridLayerContext.textAlign = 'right';
    } else {
      this.gridLayerContext.textAlign = 'center';
    }
    this.gridLayerContext.fillStyle = getConfiguration('scaleFontColor');
    for (var i = 0; i < this.data['labels'].length; i++) {
      this.gridLayerContext.save();
      if (this.rotateLabels > 0) {
        this.gridLayerContext.translate(this.yAxisPosX + i * this.labelHop, this.xAxisPosY + getConfiguration('scaleFontSize'));
        this.gridLayerContext.rotate(-(this.rotateLabels * (math.PI / 180)));
        this.gridLayerContext.fillText(this.data['labels'][i].toString(), 0, 0); 
      } else {
        this.gridLayerContext.textBaseline  = 'middle';
        this.gridLayerContext.fillText(this.data['labels'][i].toString(), this.yAxisPosX + i * this.labelHop, this.xAxisPosY + getConfiguration('scaleFontSize') + 3);
      }
      this.gridLayerContext.restore();
      this.gridLayerContext.beginPath();
      this.gridLayerContext.moveTo(this.yAxisPosX + i * this.labelHop, this.xAxisPosY + 3);

      this.gridLayerContext.save();
      //Check i isnt 0, so we dont go over the Y axis twice.
      if (getConfiguration('scaleShowGridLines') && i > 0) {
        try{
          this.gridLayerContext.setLineDash([6,4]);
        } catch(e){
          //FIXME: FF/Safari/IE 10 support http://www.rgraph.net/blog/2013/january/html5-canvas-dashed-lines.html
        }
        this.gridLayerContext.lineWidth = getConfiguration('scaleGridLineWidth');
        this.gridLayerContext.strokeStyle = getConfiguration('scaleGridLineColor');
        this.gridLayerContext.lineTo(this.yAxisPosX + i * this.labelHop, 5);
      } else {
        this.gridLayerContext.lineTo(this.yAxisPosX + i * this.labelHop, this.xAxisPosY + 3);
      }
      this.gridLayerContext.stroke();
      this.gridLayerContext.restore();
    }

    //Y axis
    this.gridLayerContext.lineWidth = getConfiguration('scaleLineWidth');
    this.gridLayerContext.strokeStyle = getConfiguration('scaleLineColor');
    this.gridLayerContext.beginPath();
    this.gridLayerContext.moveTo(this.yAxisPosX, this.xAxisPosY + 5);
    this.gridLayerContext.lineTo(this.yAxisPosX, 5);
    this.gridLayerContext.stroke();
    
    this.gridLayerContext.save();
    this.gridLayerContext.textAlign = "right";
    this.gridLayerContext.textBaseline = "middle";
    for (int j = 0; j <= calculatedScale['steps']; j++) {
      this.gridLayerContext.beginPath();
      this.gridLayerContext.moveTo(this.yAxisPosX , this.xAxisPosY - ((j + 1) * this.scaleHop));
      if (getConfiguration('scaleShowGridLines')) {
        try{
          this.gridLayerContext.setLineDash([6,4]);
        } catch(e){
          //FIXME: FF/Safari/IE 10 support http://www.rgraph.net/blog/2013/january/html5-canvas-dashed-lines.html
        }
        this.gridLayerContext.lineWidth = getConfiguration('scaleGridLineWidth');
        this.gridLayerContext.strokeStyle = getConfiguration('scaleGridLineColor');
        this.gridLayerContext.lineTo(this.width - this.widestXLabel / 2, this.xAxisPosY - ((j + 1) * this.scaleHop));
      } else {
        this.gridLayerContext.lineTo(this.yAxisPosX - 0.5, this.xAxisPosY - ((j + 1) * this.scaleHop));
      }

      this.gridLayerContext.stroke();

      if (getConfiguration('scaleShowLabels')) {
        this.gridLayerContext.fillText(calculatedScale['labels'][j], this.yAxisPosX - 8, this.xAxisPosY - (j * this.scaleHop));
      }
    }
    this.gridLayerContext.restore();
  }
  

  @override
  void populateLabels(List<Object> labels, int numberOfSteps, double graphMin, double stepSize) {
    
    //convert labels to a fixed fractionDigits length so that they look neater
    int maxDecimalPlaces = math.max(getDecimalPlaces(stepSize),getDecimalPlaces(graphMin));
    NumberFormat f = new NumberFormat.decimalPattern();
    f.minimumFractionDigits = maxDecimalPlaces;
    f.maximumFractionDigits = maxDecimalPlaces;
    
    double labelValue;
    for (int i = 0; i <= numberOfSteps; i++) {
      labelValue = (graphMin + (stepSize * i))%2; //bool values can only be either 0 or 1
      
      dynamic getLabel = getConfiguration('scaleLabel');
      
      if (getLabel != null && getLabel(labelValue, maxDecimalPlaces) != null)
        labels.add(getLabel(labelValue, maxDecimalPlaces));
      else
        labels.add(f.format(labelValue));
    }
    
  }

  
  //@override
  void drawChart(double animPc){
    
    drawBoolLines(animPc);

    
  }

}